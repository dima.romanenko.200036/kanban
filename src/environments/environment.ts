

export const environment = {
  production: false,
  firebase: {
    apiKey: 'AIzaSyATw2WcNd5VUtRnQ2ZD0i39rEjT1mQfmD0',
    authDomain: 'kanban-board-ng.firebaseapp.com',
    databaseURL: 'https://kanban-fire-workshop.firebaseio.com',
    projectId: 'kanban-board-ng',
    storageBucket: 'kanban-board-ng.appspot.com',
    messagingSenderId: '281865175075',
    appId: '1:281865175075:web:d7925e896277838ed7f196'
  },
};
