import { Component, OnInit, Output, EventEmitter  } from '@angular/core';

enum colors {
  RED = "#D32F2F",
  GREEN = "#388E3C",
  MINT = "#009886",
  BLUE = "#1976D2",
  VIOLET = "#7B1FA2",
  BROWN = "#5D4037",
  PINK = "#C2185B"
}

@Component({
  selector: 'app-color-panel',
  templateUrl: './color-panel.component.html',
  styleUrls: ['./color-panel.component.scss']
})
export class ColorPanelComponent implements OnInit {
  @Output() emitColor: EventEmitter<string> = new EventEmitter();

  colorsData = Object.values(colors)

  constructor() { }
  
  ngOnInit(): void {
  }

  onColorEmit(color: string) {
    this.emitColor.emit(color);
  }
}
