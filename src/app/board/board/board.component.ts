import { Component, OnInit } from '@angular/core';
import {
  CdkDragDrop,
  moveItemInArray,
  transferArrayItem,
} from '@angular/cdk/drag-drop';
import { BoardService } from 'src/app/services/board.service';
import { Column } from 'src/app/models/column.model';

@Component({
  selector: 'app-board',
  templateUrl: './board.component.html',
  styleUrls: ['./board.component.scss'],
})
export class BoardComponent implements OnInit {
  constructor(public boardService: BoardService) {}

  ngOnInit(): void {
    this.boardService.getInitBoard();
  }

  onColorChange(color: string, columnId: number) {
    this.boardService.changeColumnColor(color, columnId);
  }

  onAddCard(text: string, columnId: number) {
    if (text) {
      this.boardService.addCard(text, columnId);
    }
  }

  onDeleteColumn(columnId: number) {
    this.boardService.deleteColumn(columnId);
  }

  onDeleteCard(cardId: number, columnId: number) {
    this.boardService.deleteCard(cardId, columnId);
  }

  onChangeLike(event: { card: any; increase: boolean }, columnId: number) {
    const {
      card: { id },
      increase,
    } = event;
    this.boardService.changeLike(id, columnId, increase);
  }

  onAddComment(event: { id: number; text: string }, columnId: number) {
    this.boardService.addComment(columnId, event.id, event.text);
  }

  onDeleteComment(comment, columnId, item) {
    this.boardService.deleteComment(columnId, item.id, comment.id);
  }

  drop(event: CdkDragDrop<string[]>) {
    if (event.previousContainer === event.container) {
      moveItemInArray(
        event.container.data,
        event.previousIndex,
        event.currentIndex
      );
    } else {
      transferArrayItem(
        event.previousContainer.data,
        event.container.data,
        event.previousIndex,
        event.currentIndex
      );
    }
    this.boardService.dragAndDropCard();
  }

  onDownloadExcel(): void {
    const excelJsonData: {}[] = this.boardService.board.map((col: Column) =>
      this.flattenObj(col)
    );
    this.boardService.exportAsExcelFile(excelJsonData, 'retroboard');
  }

  flattenObj(ob: Column) {
    const toReturn: {} = {};
    for (const i in ob) {
      if (!ob.hasOwnProperty(i)) continue;
      if (typeof ob[i] == 'object') {
        const flatObject: {} = this.flattenObj(ob[i]);
        for (const x in flatObject) {
          if (!flatObject.hasOwnProperty(x) && x !== 'id') continue;
          toReturn[`${i}/${x}`] = flatObject[x];
        }
      } else {
        if (i !== 'id' && i !== 'color') {
          toReturn[i] = ob[i];
        }
      }
    }
    return toReturn;
  }
}
