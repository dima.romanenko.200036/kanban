import { Injectable } from '@angular/core';
import { BehaviorSubject, Observable } from 'rxjs';
import { Card, Column, Comment } from '../models/column.model';
import { AngularFirestore } from '@angular/fire/compat/firestore';
import * as fileSaver from 'file-saver';
import * as XLSX from 'xlsx';
@Injectable({
  providedIn: 'root',
})
export class BoardService {
  constructor(private firestore: AngularFirestore) {}

  private initBoard = [];
  public board: Column[] = this.initBoard;
  private board$ = new BehaviorSubject<Column[]>(this.initBoard);

  getInitBoard() {
    this.firestore
      .collection('board')
      .get()
      .subscribe((val) => {
        val.docs.forEach((doc) => this.initBoard.push(doc.data()));
      });
  }

  getBoard$() {
    return this.board$.asObservable();
  }

  changeColumnColor(color: string, columnId: number) {
    this.board = this.board.map((column: Column) => {
      if (column.id === columnId) {
        column.color = color;
        this.firestore
          .collection('board')
          .doc(columnId.toString())
          .update({ color: column.color });
      }
      return column;
    });
    this.board$.next([...this.board]);
  }

  dragAndDropCard() {
    this.board.forEach((column: Column) => {
      this.firestore
        .collection('board')
        .doc(column.id.toString())
        .update(column);
    });
  }

  addColumn(title: string) {
    const newColumn: Column = {
      id: Date.now(),
      title: title,
      color: '#009886',
      list: [],
    };
    this.firestore
      .collection('board')
      .doc(newColumn.id.toString())
      .set(Object.assign({}, newColumn));
    this.board = [...this.board, newColumn];
    this.board$.next([...this.board]);
  }

  addCard(text: string, columnId: number) {
    const newCard: Card = {
      id: Date.now(),
      text,
      like: 0,
      comments: [],
    };
    this.board = this.board.map((column: Column) => {
      if (column.id === columnId) {
        column.list = [...column.list, newCard];
        this.firestore
          .collection('board')
          .doc(columnId.toString())
          .update({ list: column.list });
      }
      return column;
    });
    this.board$.next([...this.board]);
  }

  deleteColumn(columnId) {
    this.board = this.board.filter((column: Column) => column.id !== columnId);
    this.firestore.collection('board').doc(columnId.toString()).delete();
    this.board$.next([...this.board]);
  }

  deleteCard(cardId: number, columnId: number) {
    this.board = this.board.map((column: Column) => {
      if (column.id === columnId) {
        column.list = column.list.filter((card: Card) => card.id !== cardId);
        this.firestore
          .collection('board')
          .doc(columnId.toString())
          .update({ list: column.list });
      }
      return column;
    });
    this.board$.next([...this.board]);
  }

  changeLike(cardId: number, columnId: number, increase: boolean) {
    this.board = this.board.map((column: Column) => {
      if (column.id === columnId) {
        const list = column.list.map((card: Card) => {
          if (card.id === cardId) {
            if (increase) {
              if (localStorage.getItem(card.id.toString()) !== 'liked') {
                card.like++;
                localStorage.setItem(card.id.toString(), 'liked');
              }
            } else {
              if (
                card.like > 0 &&
                localStorage.getItem(card.id.toString()) !== 'unliked'
              ) {
                card.like--;
                localStorage.setItem(card.id.toString(), 'unliked');
              }
            }
          }
          return card;
        });
        this.firestore
          .collection('board')
          .doc(columnId.toString())
          .update({ list: list });
        column.list = list;
        return column;
      } else {
        return column;
      }
    });
    this.board$.next([...this.board]);
  }

  addComment(columnId: number, cardId: number, text: string) {
    this.board = this.board.map((column: Column) => {
      if (column.id === columnId) {
        const list = column.list.map((card: Card) => {
          if (card.id === cardId) {
            const newComment = {
              id: Date.now(),
              text,
            };
            card.comments = [...card.comments, newComment];
          }
          return card;
        });
        this.firestore
          .collection('board')
          .doc(columnId.toString())
          .update({ list: list });
        column.list = list;
      }
      return column;
    });
    this.board$.next([...this.board]);
  }

  deleteComment(columnId: number, itemId: number, commentId: number) {
    this.board = this.board.map((column: Column) => {
      if (column.id === columnId) {
        const list = column.list.map((item: Card) => {
          if (item.id === itemId) {
            item.comments = item.comments.filter((comment: Comment) => {
              return comment.id !== commentId;
            });
          }
          return item;
        });
        this.firestore
          .collection('board')
          .doc(columnId.toString())
          .update({ list: list });
        column.list = list;
      }
      return column;
    });
    this.board$.next([...this.board]);
  }

  exportAsExcelFile(json: any[], excelFileName: string): void {
    const worksheet: XLSX.WorkSheet = XLSX.utils.json_to_sheet(json);
    const workbook: XLSX.WorkBook = {
      Sheets: { data: worksheet },
      SheetNames: ['data'],
    };
    const excelBuffer: any = XLSX.write(workbook, {
      bookType: 'xlsx',
      type: 'array',
    });
    this.saveAsExcelFile(excelBuffer, excelFileName);
  }

  saveAsExcelFile(buffer: any, fileName: string): void {
    const data: Blob = new Blob([buffer], {
      type: 'application/vnd.openxmlformats-officedocument.spreadsheetml.sheet;charset=UTF-8',
    });
    fileSaver.saveAs(data, `${fileName}_export_${new Date().getTime()}.xlsx`);
  }
}
